# base-builder

The base container repo for CI pipeline images. The main focus of this image is to optimize the build times  
by including certain tools which are currently being installed everytime in `before_script` sections.  
These tools include:

- curl
- helm
- kubectl
- terraform
- make
- sipcalc
- civo terraform provider (0.9.15)
