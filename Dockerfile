FROM docker

# install curl
RUN apk add curl

# install helm
ARG HELM_VERSION
RUN wget "https://get.helm.sh/${HELM_VERSION}"
RUN tar -zxvf "${HELM_VERSION}"
RUN cp ./linux-amd64/helm /usr/bin

# install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

# install terraform
RUN curl https://releases.hashicorp.com/terraform/0.12.26/terraform_0.12.26_linux_amd64.zip -O
RUN unzip terraform_0.12.26_linux_amd64.zip
RUN mv terraform /usr/local/bin/terraform

# install civo terraform provider plugin
RUN curl -L https://github.com/civo/terraform-provider-civo/releases/download/v0.9.15/terraform-provider-civo_0.9.15_linux_amd64.tar.gz -O
RUN tar -zxvf terraform-provider-civo_0.9.15_linux_amd64.tar.gz

# install make
RUN apk add make

# install sipcalc
RUN apk add sipcalc
